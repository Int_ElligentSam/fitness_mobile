import 'package:fitness_mobile/routes/login.dart';
import 'package:fitness_mobile/routes/signup.dart';

import './routes/welcome.dart';
import 'package:flutter/material.dart';

void main() => runApp(FitnessMobileApp());

class FitnessMobileApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Power Fitness",
      initialRoute: "/",
      routes: {
        "/": (context) => WelcomeScreen(),
        "/login": (context) => LoginScreen(),
        "/sign-up": (context) => SignUpScreen()
      },
    );
  }
}
