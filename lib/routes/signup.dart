import 'package:flutter/material.dart';

class SignUpScreen extends StatefulWidget {
  SignUpScreen({Key key}) : super(key: key);

  _SignUpScreebState createState() => _SignUpScreebState();
}

class _SignUpScreebState extends State<SignUpScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Hero(
                tag: "logo",
                child: Center(
                  child: Image(
                    image: AssetImage("assets/logo.jpg"),
                    width: MediaQuery.of(context).size.width * 0.80,
                    height: MediaQuery.of(context).size.height * 0.30,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 16.0),
                child: Text(
                  "Shape\nYour Body",
                  style: TextStyle(
                    fontSize: 40,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Center(
                child: Padding(
                  padding: const EdgeInsets.all(18.0),
                  child: Material(
                    elevation: 4.0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(10),
                      ),
                    ),
                    color: Colors.white,
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.symmetric(
                            horizontal: 18.0,
                          ),
                          child: TextField(
                            decoration: InputDecoration(
                              icon: Icon(Icons.account_circle),
                              border: InputBorder.none,
                              hintText: "Full Name",
                            ),
                          ),
                        ),
                        Divider(),
                        Padding(
                          padding: const EdgeInsets.symmetric(
                            horizontal: 18.0,
                          ),
                          child: TextField(
                            decoration: InputDecoration(
                              icon: Icon(Icons.mail_outline),
                              border: InputBorder.none,
                              hintText: "Enter your Email",
                            ),
                          ),
                        ),
                        Divider(),
                        Padding(
                          padding: const EdgeInsets.symmetric(
                            horizontal: 18.0,
                          ),
                          child: TextField(
                            obscureText: true,
                            decoration: InputDecoration(
                              icon: Icon(Icons.lock_outline),
                              border: InputBorder.none,
                              hintText: "* * * * * * * * * *",
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Center(
                  child: MaterialButton(
                    padding: EdgeInsets.symmetric(
                      horizontal: 60.0,
                    ),
                    onPressed: () {},
                    child: Text("Sign Up"),
                    color: Colors.deepOrangeAccent,
                    textColor: Colors.white,
                    shape: StadiumBorder(),
                  ),
                ),
              ),
              Center(
                child: MaterialButton(
                  onPressed: () {},
                  shape: StadiumBorder(),
                  child: Text("Already have an account? Login"),
                  textColor: Colors.grey[500],
                ),
              ),
              Spacer(),
              Hero(
                tag: "indicator",
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 12.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      TabPageSelectorIndicator(
                        backgroundColor: Colors.grey,
                        size: 8,
                        borderColor: Colors.blue,
                      ),
                      TabPageSelectorIndicator(
                        backgroundColor: Colors.grey,
                        size: 8,
                        borderColor: Colors.blue,
                      ),
                      TabPageSelectorIndicator(
                        backgroundColor: Colors.deepOrangeAccent,
                        size: 12,
                        borderColor: Colors.blue,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
