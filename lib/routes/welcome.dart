import 'package:flutter/material.dart';

class WelcomeScreen extends StatelessWidget {
  const WelcomeScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Hero(
            tag: "logo",
            child: Center(
              child: Image(
                image: AssetImage("assets/logo.jpg"),
                width: MediaQuery.of(context).size.width * 0.80,
                height: MediaQuery.of(context).size.height * 0.30,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 16.0),
            child: Text(
              "Personalized\nTraining",
              style: TextStyle(
                fontSize: 40,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Center(
            child: Container(
              width: MediaQuery.of(context).size.width * 0.70,
              child: Padding(
                padding: const EdgeInsets.only(top: 42.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    MaterialButton(
                      onPressed: () {
                        Navigator.of(context).pushNamed("/login");
                      },
                      color: Colors.purple,
                      shape: StadiumBorder(),
                      textColor: Colors.white,
                      child: Text("Login"),
                    ),
                    SizedBox(
                      height: 12,
                    ),
                    MaterialButton(
                      onPressed: () {
                        Navigator.of(context).pushNamed("/sign-up");
                      },
                      color: Colors.deepOrange,
                      shape: StadiumBorder(),
                      textColor: Colors.white,
                      child: Text("Sign Up"),
                    ),
                    SizedBox(
                      height: 12,
                    ),
                    MaterialButton(
                      onPressed: () {},
                      color: Colors.deepPurple,
                      shape: StadiumBorder(),
                      textColor: Colors.white,
                      child: Text("Connect with others"),
                    )
                  ],
                ),
              ),
            ),
          ),
          Spacer(),
          Hero(
            tag: "indicator",
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 12.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  TabPageSelectorIndicator(
                    backgroundColor: Colors.deepPurple,
                    size: 12,
                    borderColor: Colors.blue,
                  ),
                  TabPageSelectorIndicator(
                    backgroundColor: Colors.grey,
                    size: 8,
                    borderColor: Colors.blue,
                  ),
                  TabPageSelectorIndicator(
                    backgroundColor: Colors.grey,
                    size: 8,
                    borderColor: Colors.blue,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
