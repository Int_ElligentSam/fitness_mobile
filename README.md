# fitness_mobile

A Flutter UI challenge inspired by [Fitness Mobile Login](https://www.uplabs.com/posts/fitness-mobile-login-page) at Uplabs.


## Getting Started

Simply clone the repo and run

```bash
flutter pub get
```
In the app directory.

## ScreenShots
<div style="display: flex; flex-direction: row; justify-content: space-around; align-items: center;  flex-wrap: wrap;">
    <img src="./assets/screenshots/flutter_01.png" width="300" height="600"/>
    <img src="./assets/screenshots/flutter_02.png" width="300" height="600"/>
    <img src="./assets/screenshots/flutter_03.png" width="300" height="600"/>
</div>

